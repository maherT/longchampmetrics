﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Longchamp
{
    class ScenariosWeb : FixturesWeb
    {
        bool actionDone = false;
        [Test, Order(1)]
        [TestCase(TestName = "Longchamp")]
        public void TestSimple()
        {
            // visiter la page longchamp
           _driver.Navigate().GoToUrl("https://fr.longchamp.com");
           
            Harf();
            HttpResponse(_driver.Url);
            //cliquer rechercher
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#block-searchtooltip > a > span"))).Click();
            //saisir sacs
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("edit-search-api-fulltext"))).SendKeys("sacs");
            //rechercher
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("edit-submit-lc-search"))).Click();
            Harf();
            HttpResponse(_driver.Url);
            WebClient client = new WebClient();
            Stream downloadString = client.OpenRead(_driver.Url);
            //cliquer sur uu produit
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div > div > div > div > a:nth-child(3) > div > figure > img"))).Click();
            //choisir la couleur marron

            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > ul > li:nth-child(1) > a"))).Click();
            //choisir la couleur bleue foncé
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > ul > li:nth-child(2) > a"))).Click();
            //choisir la couleur jaune
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > ul > li:nth-child(3) > a"))).Click();
            //choisir la coulueur noir
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > ul > li:nth-child(5) > a"))).Click();
            //choisir la taille
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > button"))).Click();
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#choose-size > ul > li:nth-child(1) > a"))).Click();
            //ajouter le produit au panier
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > a.btn.btn--symbol.btn--symbol--m.btn--full.btn--alpha.btn--alpha-reverse.btn--s.upper.cart-ajax"))).Click();
            //valider la commande
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#tooltip-cart > div.p-1 > a"))).Click();
            //offrir un cadeau
            Harf();
            HttpResponse(_driver.Url);
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("label_expand_1"))).Click();
           
                //rediger un message
               // _wait.Until(ExpectedConditions.ElementToBeClickable(By.Name("items[product_L1366800504_1][gift][text]"))).SendKeys("hello my best friend how are you!!this is for you");

                //previsionner le résultat
              //  _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#label_modal_2"))).Click();
                Thread.Sleep(3000);

                //close 
              //  _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#js-modal-close > span"))).Click();
                //enregistrer 
               // _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#edit-items-product-l1320870001-1-gift-submit-gift-product-l1320870001-1"))).Click();
           
         
            // partie homme 
            //cliquer sur homme
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#link-homme > a"))).Click();
            Harf();
            HttpResponse(_driver.Url);
            //scroller et cliquer le produit

            IWebElement lit = _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div.section.section--category.center > div > div > a:nth-child(1) > div > span")));
            while (lit.Displayed && actionDone == false)
            {
                js.ExecuteScript(String.Format("window.scrollBy({0}, ({1}/4))", 0, "window.innerHeight"));
                try
                {
                    lit.Click();
                    actionDone = true;
                    break;
                }
                catch (Exception)
                {
                }
            }
            Thread.Sleep(3000);
            //choisir voir toud les produits
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-result > div.prd-cell__header > a > span.display-l--from"))).Click();
            //cliquer ser la couleur
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#filter-by-tooltip > li:nth-child(3) > button"))).Click();
            //choisir la couleur marron
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#filter__col__color > label:nth-child(1) > span > span"))).Click();
            //cliquer sur le 1er produits 
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-result > div > div > a:nth-child(1) > div > figure > img"))).Click();
            //trouver un magasin
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#skip-main > div > div > div.prd-txt > a:nth-child(7)"))).Click();
            Thread.Sleep(5000);
            //choisir paris 
            _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("storeloc-search-input"))).SendKeys("Paris");
            //cherhcer
           _wait.Until(ExpectedConditions.ElementToBeClickable(By.CssSelector("#search-store-locator-form > button.btn.btn--submit > svg"))).Click();
            Thread.Sleep(4000);
            //zoomer 3 fois
            int n = 1;
            while (n < 3)
            {
                _wait.Until(ExpectedConditions.ElementToBeClickable(By.Id("ZoomInButton"))).Click();
                n++;
            }        
            Thread.Sleep(3000);
        }
    }
}
