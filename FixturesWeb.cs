﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Longchamp
{
    class FixturesWeb
    {
        public IJavaScriptExecutor js;
        public IWebDriver _driver;
        public WebDriverWait _wait;
        public List<List<Ressource>> _PagesToTest;
        public List<string> UrlsToTest = new List<string>
            {   // Add list of urls to check!
                // always set the first index of the home page
               "https://testingdigital.com/fr/contact",
               "https://free.fr"
            };
        public Actions _ac;
        [SetUp]
        public void SetUp()
        {
            // Lancer le Nav
            _driver = new ChromeDriver(@"C:\WEBDRIVERS");
            // Plein Ecran
            _driver.Manage().Window.Maximize();
            // Wait pour les elements
            _wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(20));
            js = (IJavaScriptExecutor)_driver;
            _ac = new Actions(_driver);
            _PagesToTest = PagesRessource();
        }
        public void Harf()
        {
            long tempsDeChargement = (long)js.ExecuteScript("return window.performance.timing.loadEventEnd") - (long)js.ExecuteScript("return window.performance.timing.navigationStart");
            long tempsRedirection = (long)js.ExecuteScript("return window.performance.timing.redirectEnd") - (long)js.ExecuteScript("return window.performance.timing.redirectStart");
            long tempsDeRequestsResponses = (long)js.ExecuteScript("return window.performance.timing.responseEnd") - (long)js.ExecuteScript("return window.performance.timing.requestStart");
            long tempsProcessing = (long)js.ExecuteScript("return window.performance.timing.domComplete") - (long)js.ExecuteScript("return window.performance.timing.domLoading");
            string title = (string)js.ExecuteScript("return document.title");
            //string memory = (string)js.ExecuteScript("return window.performance.memory");
            //---
            Console.WriteLine("------------------------------- Performance --------------------------------");
            Console.WriteLine("Titre de la page: [" + title + "]");
            Console.WriteLine("Temps de chargement de la page: [" + tempsDeChargement + "]ms");
           // Console.WriteLine("Temps de traitements Requetes et Reponses: [" + tempsDeRequestsResponses + "]ms");
            Console.WriteLine("Temps de redirection entre les pages : [" + tempsRedirection + "]ms");
           // Console.WriteLine("information mémoire : [" + memory + "]ms");

        }

        [TearDown]
        public void TearDown()
        {
            // Fermer le navigateur
            _driver.Close();
        }
        public HttpWebResponse HttpResponse(string URL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            // Set credentials to use for this request.
            request.Credentials = CredentialCache.DefaultCredentials;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            // Get the stream associated with the response.
            Stream receiveStream = response.GetResponseStream();
            // Pipes the stream to a higher level stream reader with the required encoding format. 
            StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
            Console.WriteLine("------------------------------ HTTP Response --------------------------------");
            // Console.WriteLine("Etat de connexion:  ["+response.StatusCode+"]");
            Console.WriteLine("Type des méthodes:" + response.Method);
            Console.WriteLine("Adresse de la page actuelle:" + request.Address);
            Console.WriteLine("Les entetes des pages :" + response.CharacterSet);
            Console.WriteLine("Dernières executions:" + response.LastModified);

            response.Close();
            readStream.Close();
            return response;
        }


        public MatchCollection RegX(string Input, string Regex)
        {
            var regex = new Regex(Regex);
            return regex.Matches(Input);
        }
        public List<Ressource> SetupData(string mode, List<string> listUrl)
        {
            List<Ressource> allMetrics = new List<Ressource>();
            foreach (var url in listUrl)
            {
                _driver.Navigate().GoToUrl(url); // Set Session test destination.
                HttpResponse(url);

                IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
                #region Get online Ressource of the progressive Web App
                Ressource _Ressources = new Ressource
                {
                    TestType = mode, //online|offline
                    Meta = RegX(_driver.PageSource.ToString(), @"<meta (.*?)\/>"), //  meta tags.
                    Link = RegX(_driver.PageSource.ToString(), @"<link (.*?)\/>"), // link tags.
                    Img = RegX(_driver.PageSource.ToString(), @"<img (.*?)\/>"), // img tags.
                    allTagProp = RegX(_driver.PageSource.ToString(), "(\\S+)=[\"']?((?:.(?![\"']?\\s+(?:\\S+)=|[>\"']))+.)[\"']?"),   
                };
                allMetrics.Add(_Ressources);
                #endregion
                // to quick check Some information of running online/offline instance.
                //PrintMetrics(_Ressources, mode);
                
            }
            return allMetrics;
        }
        public List<List<Ressource>> PagesRessource()
        {
            List<List<Ressource>> _ElementsToTest = new List<List<Ressource>>();
            // getting App data online mode
            _ElementsToTest.Add(SetupData("ONLINE", UrlsToTest));
            Thread.Sleep(15000);
            // Turn Off Wifi.
            
            // getting App data offline mode
            _ElementsToTest.Add(SetupData("OFFLINE", UrlsToTest));
            return _ElementsToTest;
        }


        // lets print something ! :D
        public void PrintMetrics(Ressource dataMetric, String mode)
        {
            Console.WriteLine("----=========---- " + mode + " MODE ----=========----\n");
            Console.WriteLine("Page : " + dataMetric.Url);
            Console.WriteLine("Original String : " + dataMetric.ServerResponse.ResponseUri.OriginalString);
            Console.WriteLine("Load time : " + (dataMetric.LoadEventEnd - dataMetric.LoadStart + "milliseconds"));
            Console.WriteLine("Server Status Code : " + dataMetric.ServerResponse.StatusCode);
            Console.WriteLine("Authority : " + dataMetric.ServerResponse.ResponseUri.Authority);
            Console.WriteLine("Idn Host : " + dataMetric.ServerResponse.ResponseUri.IdnHost);
            Console.WriteLine("Host name Type : " + dataMetric.ServerResponse.ResponseUri.HostNameType);
            Console.WriteLine("Scheme : " + dataMetric.ServerResponse.ResponseUri.Scheme);
            Console.WriteLine("Port : " + dataMetric.ServerResponse.ResponseUri.Port + "\n");
        }
    }
}    

